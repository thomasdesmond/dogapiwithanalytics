﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ApiInClassDemo.Models;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace ApiInClassDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    Spinner.IsRunning = true;
                    string dogApiEndpoint = "https://dog.ceo/api/breeds/image/random";
                    Uri dogApiUri = new Uri(dogApiEndpoint);
    
                    HttpClient client = new HttpClient();
                    DogImage dogData = new DogImage();

                    throw new Exception("CS 481 is the best!");
    
                    HttpResponseMessage response = await client.GetAsync(dogApiUri);
                    if (response.IsSuccessStatusCode)
                    {
                        Analytics.TrackEvent("Successful Dog API Request", new Dictionary<string, string> {
                            { "Response Code", response.StatusCode.ToString()},
                          });
    
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        dogData = JsonConvert.DeserializeObject<DogImage>(jsonContent);
                        DogPhoto.Source = dogData.Message;
                    }
                    else
                    {
                        Analytics.TrackEvent("Unsuccessful Dog API Request");
                    }
                    Spinner.IsRunning = false;
                
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex);
                    await DisplayAlert("Error", "Could not get dog picture", "OK");
                }
                finally
                {
                    Spinner.IsRunning = false;
                }

            }
            else
            {
                await DisplayAlert("No Internet", "Could not connect to the internet", "close");
            }
        }
    }
}
